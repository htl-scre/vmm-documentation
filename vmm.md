# VMM

VMM dient der Notenverwaltung und ermöglicht Schülern, Lehrern und Eltern(wenn sie das Passwort kennen) Zugriff auf den
aktuellen Notenstand.  
VMM erstellt Noten**vorschläge**, die keineswegs bindend sind

### Login

![](login.png)
Das Login erfolgt mit jenen Informationen, mit denen man sich auch bei den Klassennotebooks anmeldet, also z.B.:  
Username: `RAWO`  
Password: `n4jEhWE@YdqPprNn`

### Gruppenverwaltung

![](gruppenverwaltung.png)
Eine Gruppe entspricht einigen Schülern, meist der ganzen Klasse. In dieser Sektion können z.B. Schüler entfernt werden,
die sich abmelden.

Gruppen können direkt aus der Lehrfächerverteilung importiert werden:
![](lfv-anlegen.png)

Mehrere Lehrer können sich eine Gruppe teilen, sodass alle Noten geben können und eine gemeinsame Note nach einer
festgelegten Gewichtung errechnet werden kann.
![](shared-group.png)

### Maximale Negativausprägung

Wie *schlimm* ist ein 0 Punkte Test(Notenberechnung linear)?  
Empfohlener Wertebereich: [5.5, 7], Nachjustierung während des Schuljahres möglich

### Kategorienverwaltung

VMM errechnet die Note durch einen [gewichteten Mittelwert](https://de.wikipedia.org/wiki/Gewichtung). Beispielsweise

* Schularbeit mit Gewichtung 10: Note 2
* HÜ mit Gewichtung 5: Note 4
* HÜ mit Gewichtung 5: Note 4  
  **--> Gesamtnote 3**

![](kategorie.png)

* Gegenstände: Für welche Gegenstände diese Kategorie verfügbar sein soll
* Gesetzeskategorie: Was für eine Art Note ist es - Schularbeit, Schriftliche Leistungsfestellung, ... ; **für Schüler
  sichtbar**
* Maximale Gesamtgewichtung: Muss mit dem Toggle "Deckelung" aktiviert werden; *für Schüler nicht sichtbar*
* Gewichtung pro Arbeit: Faktor mit welchem die Note beim gewichteten Mittelwert multipliziert wird, je höher umso
  wichtiger die Note; *für Schüler nicht sichtbar* z.B.
    * 25 für Schularbeit
    * 5 für HÜ
    * 3 für Mitarbeit
* Eingabeform; *für Schüler nicht sichtbar*
    * Noten: Noten 1-5 können eingetragen werden
    * Punkte: Es werden Punkte eingetragen(variable Notengrenzen)
* Ausgabeform; **für Schüler sichtbar**
    * Noten: Schüler sieht 1-5, unabhängig von der `Eingabeform`
    * Zeichen: Schüler sieht `+~, ~, ... , -`, unabhängig von der `Eingabeform`
* Notenberechnung, Annahme für Erklärung: Eingabe Punkte, 100 Punkte, ab 90 *Sehr gut*
    * Linear:
        * 100 Punkte führt zu einer Note von 0.5
        * 95 Punkte führt zu einer Note von 1
        * 90 Punkte führt zu einer Note von 1.5
    * Punktuell:
        * 90=91=92=...=99=100 Punkte führt zu einer Note von 1
    * Öffentlich: Falls angewählt ist die Kategorie im Kategorienpool für alle Kollegen verfügbar

![](kategorienpool.png)
Ermöglicht Import von Kategorien anderer Lehrer, Anpassung möglich

### Notenverwaltung

Noten können für einen einzelnen Schüler oder für eine ganze Gruppe eingetragen werden; an dieser Stelle wird nur der
Gruppeneintrag erklärt, da sich das Eintragen bei einem einzelnen Schüler bzw. bei einem zufälligen nicht unterscheidet.

![](gruppeneintrag.png)

* Kategorie: Alle Kategorien, welche für diesen Gegenstand angewählt wurden sind auswählbar
* Kompetenzbereich: Kann ausgefüllt werden, sind nur für wenige Gegenstände eingetragen, bei Bedarf bitte melden
* Öffentliche Bemerkung: **Für Schüler sichtbar**, dient der Übersicht, kann *global* gesetzt werden
* Private Bemerkung: *Für Schüler nicht sichtbar*
* Beurteilung: Je nach `Eingabeform` können hier Punkte oder Noten eingetragen werden.  
  **Unterstützt spaltenweises copy-paste z.B. aus Excel**
* Flag 5: Solang der Schüler mindestens einen Eintrag mit Flag 5 hat steht er auf 5, egal was er sonst für Einträge hat.

Fragen, Wünsche, Beschwerden gern bei einem Kaffee, Teams oder Email an [SCRE](mailto:christoph.schreiber@htlstp.ac.at?subject=VMM)